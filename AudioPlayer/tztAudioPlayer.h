//
//  AudioPlayer.h
//  Share
//
//  Created by Lin Zhang on 11-4-26.
//  Copyright 2011年 www.eoemobile.com. All rights reserved.
//

#import <Foundation/Foundation.h>

//url 播放类型
typedef enum : NSUInteger {
    tztAudioTypeUnkown,
    tztAudioTypeRecord,
    tztAudioTypeLive,
} tztAudioType;

// 播放状态
typedef enum : NSUInteger {
    tztAudioPlayerStatusUnkown,
    tztAudioPlayerStatusReadyToPlay,
    tztAudioPlayerStatusPlaying,
    tztAudioPlayerStatusBuffering,
    tztAudioPlayerStatusPaused,
    tztAudioPlayerStatusFinished,
} tztAudioPlayerStatus;

@interface tztAudioPlayer : NSObject

@property (nonatomic, readonly) tztAudioPlayerStatus status;
@property (nonatomic, readonly) NSTimeInterval duration;

+ (instancetype)sharedInstance;

//该方法初始化数据的同时，会重置播放器状态，停止播放正在播放的音频，请在确定要切换音频数据源的时候才调用，
- (void)prepareToPlayWithUrl:(NSString *)url audioType:(tztAudioType)type audioName:(NSString *)name;

//从音频起始位置开始播放
- (void)play;

//从指定位置播放音频
- (void)playWithStartTime:(NSTimeInterval)startTime;

//暂停
- (void)pause;

//移动音频的播放位置
- (void)seekToTime:(float)time completion:(void(^)(BOOL success))comp;

//停止播放，会销毁当前播放器，如果要重新播放 需要重新调用 prepareToPlayWithUrl 方法
- (void)stop;

//重置播放器状态到 0,如果此时正在播放，会停止播放音频
- (void)resetPlayer;

//回调 进度、播放状态、首次获取到duration
- (void)addObserver:(id)observer;
- (void)removeObserver:(id)observer;

//播放音频名称 外部作为键值对使用，用来标记当前播放的音频key
- (NSString *)getAudioName;
//直播，录播，其他
- (tztAudioType)getAudioType;

//以下方法 不要调用，是用来回调给observer的
- (void)tztAudioPlayerDidGetDuraion:(NSTimeInterval)duration audioType:(tztAudioType)type audioName:(NSString *)name;
- (void)tztAudioPlayerStatusDidChanged:(tztAudioPlayerStatus)status audioType:(tztAudioType)type audioName:(NSString *)name;
- (void)tztAudioPlayerProgressDidUpdated:(NSTimeInterval)progress audioType:(tztAudioType)type audioName:(NSString *)name;

@end
