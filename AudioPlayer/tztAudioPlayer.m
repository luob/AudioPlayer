//
//  AudioPlayer.m
//  Share
//
//  Created by Lin Zhang on 11-4-26.
//  Copyright 2011年 www.eoemobile.com. All rights reserved.
//

#import "tztAudioPlayer.h"
#import "tztAudioStreamer.h"
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import <objc/runtime.h>
#import <CoreMedia/CoreMedia.h>

@interface tztAudioPlayer () {
    NSURL *_url;
    NSTimer *timer;
    tztAudioType _audioType;
    NSString *_audioName;
    
    NSMutableArray *_observers;
}
@property (nonatomic, retain) tztAudioStreamer *streamer;

@end

@implementation tztAudioPlayer
static tztAudioPlayer *_sharedPlayer = nil;
+ (instancetype)sharedInstance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedPlayer = [[tztAudioPlayer alloc] init];
    });
    return _sharedPlayer;
}

- (id)init {
    self = [super init];
    if (self) {
        [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
        [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
    }
    return self;
}

- (void)dealloc
{
    [_url release];
    [_streamer release];
    [timer invalidate];
    [super dealloc];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {

}

- (void)remoteEventTypeValueChanged:(NSNotification *)notification {
    
    NSInteger subType = [notification.object integerValue];
    switch (subType) {
        case UIEventSubtypeRemoteControlPause:
            // 暂停
            [self pause];
            break;
        case UIEventSubtypeRemoteControlPlay:
            // 播放
            [self play];
            break;
        case UIEventSubtypeRemoteControlTogglePlayPause:
            // 耳机播放或暂停切换
            if (_status == tztAudioPlayerStatusPlaying) {
                [self pause];
            } else {
                [self play];
            }
            break;
        default:
            break;
    }
}

- (void)getDurationWithCompletion:(void(^)(NSTimeInterval duration))comp {
    NSDictionary *options = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:YES] forKey:AVURLAssetPreferPreciseDurationAndTimingKey];
        AVURLAsset *audioAsset = [[[AVURLAsset alloc] initWithURL:_url options:options] autorelease];
    NSArray *keys = [[NSArray alloc] initWithObjects:@"duration", nil];
    [audioAsset loadValuesAsynchronouslyForKeys:keys completionHandler:^{
        NSError *error = nil;
        switch ([audioAsset statusOfValueForKey:@"duration" error:&error]) {
                
            case AVKeyValueStatusLoaded:{
                CMTime audioDuration = audioAsset.duration;
                
                NSTimeInterval duration = CMTimeGetSeconds(audioDuration);
                 if (comp) {
                     dispatch_async(dispatch_get_main_queue(),^{
                         comp(duration);
                     });
                }
            }
                // duration is now known, so we can fetch it without blocking
//                CMTime duration = [asset duration];
                // dispatch a block to the main thread that updates the display of asset duration in my user interface,
                // or do something else interesting with it
            default: ;
                // something went wrong; depending on what it was, we may want to dispatch a
                // block to the main thread to report th         e error
        }
    }];
}

#pragma mark - public
//该方法初始化数据的同时，会重置播放器状态，停止播放正在播放的音频，请在确定要切换音频数据源的时候才调用，
- (void)prepareToPlayWithUrl:(NSString *)url audioType:(tztAudioType)type audioName:(NSString *)name {
    if (url.length == 0) {
        return;
    }
    _duration = -1;
    _url = [NSURL URLWithString:url];
    
    [self getDurationWithCompletion:^(NSTimeInterval duration) {
        NSLog(@"did get duration with asset url :%lf",duration);
        if (duration > 0) {
            _duration = duration;
            [self invokeDurationToObserver:_duration];
        }
    }];
    
    if (_streamer) {
        [_streamer stop];
        [_streamer release];
        _streamer = nil;
    }
    _audioType = type;
    _audioName = name;
    
    self.streamer = [[tztAudioStreamer alloc] initWithURL:_url];
    
    // register the streamer on notification
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playbackStateChanged:)
                                                 name:ASStatusChangedNotification
                                               object:_streamer];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playbackDurationChanged:)
                                                 name:ASDurationDidUpdateNotification
                                               object:_streamer];
    NSString *notificationName = @"remoteControlReceivedWithEvent";
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(remoteEventTypeValueChanged:)
                                                 name:notificationName
                                               object:nil];
    
    if (timer) {
        if ([timer respondsToSelector:@selector(invalidate)]){
            [timer invalidate];
            timer = nil;
        }
    }
    timer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                             target:self
                                           selector:@selector(updateProgress)
                                           userInfo:nil repeats:YES];
    
    _status = tztAudioPlayerStatusReadyToPlay;
}

//正常从 0 开始播放
- (void)play {
    if (!_streamer) {
        return;
    }
    [_streamer start];
}

//从指定位置播放音频
- (void)playWithStartTime:(NSTimeInterval)startTime {
    if (!_streamer) {
        return;
    }
    if (startTime <= 0) {
        [_streamer start];
    }
    [_streamer startWithOffTime:startTime];
}

//暂停播放
- (void)pause {
    if (!_streamer) {
        return;
    }
    [_streamer pause];
}

//移动音频的播放位置
- (void)seekToTime:(float)time completion:(void(^)(BOOL success))comp {
    if (!_streamer) {
        if (comp) {
            comp(NO);
        }
        return;
    }
    if ((long)_duration <= 0) {
        if (comp) {
            comp(NO);
        }
        return;
    }
    if ((long)time < 0 || (long)time > (long)_duration) {
        if (comp) {
            comp(NO);
        }
        return;
    }
    //拖到最后 直接stop
    if ((long)time == (long)_duration) {
        NSLog(@"seek to end ，will stop ...");
        //回调 状态
        [self stop];
        [self invokeStatusToObserver:tztAudioPlayerStatusFinished];
        if (comp) {
            comp(YES);
        }
        return;
    }
    [_streamer seekToTime:time completion:comp];
}

//停止播放，释放播放器资源
- (void)stop {
    // release streamer
    if (_streamer) {
		[_streamer stop];
		[_streamer release];
		_streamer = nil;
        
        // remove notification observer for streamer
		[[NSNotificationCenter defaultCenter] removeObserver:self 
                                                        name:ASStatusChangedNotification
                                                      object:_streamer];
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:ASDurationDidUpdateNotification
                                                      object:_streamer];
        
        NSString *notificationName = @"remoteControlReceivedWithEvent";
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:notificationName
                                                      object:nil];
        if (timer) {
            if ([timer respondsToSelector:@selector(invalidate)]){
                [timer invalidate];
                timer = nil;
            }
        }
	}
    _status = tztAudioPlayerStatusUnkown;
}

//重置播放器状态到 0
- (void)resetPlayer {
    if (_streamer) {
        [self stop];
    }
    [self prepareToPlayWithUrl:_url.absoluteString audioType:_audioType audioName:_audioName];
}

#pragma mark - private
- (void)updateProgress {
    NSTimeInterval progress = -1;
    if (_streamer.progress > 0.0 ) {
        progress = _streamer.progress;
    } else {
        progress = 0.0;
    }
    [self invokeProgressToObserver:progress];
}

- (void)playbackStateChanged:(NSNotification *)notification {
    tztAudioPlayerStatus status = tztAudioPlayerStatusUnkown;
    if ([_streamer isWaiting])
	{
        status = tztAudioPlayerStatusBuffering;
    } else if ([_streamer isIdle]) {
        status = tztAudioPlayerStatusFinished;
        //播放结束，自动销毁播放器
        [self stop];
	} else if ([_streamer isPaused]) {
        status = tztAudioPlayerStatusPaused;
    } else if ([_streamer isPlaying] || [_streamer isFinishing]) {
        status = tztAudioPlayerStatusPlaying;
	} else {
        
    }
    _status = status;
    [self invokeStatusToObserver:status];
}

- (void)playbackDurationChanged:(NSNotification *)nitification {
    if (_streamer.duration > 0) {
        if (_duration < 0) {
            _duration = _streamer.duration;
            [self invokeDurationToObserver:_duration];
        }
    }
}

#pragma mark - get funcs
- (NSString *)getAudioName {
    return _audioName;
}

- (tztAudioType)getAudioType {
    return _audioType;
}

#pragma mark - invoke callback
//回调 进度、播放状态、首次获取到duration
- (void)addObserver:(id)observer {
    if (!_observers) {
        _observers = [[NSMutableArray alloc] initWithCapacity:1];
    }
    if ([_observers containsObject:observer]) {
        return;
    }
    [_observers addObject:observer];
}
- (void)removeObserver:(id)observer {
    if (_observers.count == 0) {
        return;
    }
    if (![_observers containsObject:observer]) {
        return;
    }
    [_observers removeObject:observer];
}

- (void)invokeDurationToObserver:(NSTimeInterval)duration {
    for (id target in _observers) {
        SEL selector = @selector(tztAudioPlayerDidGetDuraion:audioType:audioName:);
        if (target && [target respondsToSelector:selector]) {
            objc_msgSend(target,selector,duration,_audioType,_audioName);
        }
    }
}

- (void)invokeStatusToObserver:(tztAudioPlayerStatus)status {
    for (id target in _observers) {
        SEL selector = @selector(tztAudioPlayerStatusDidChanged:audioType:audioName:);
        if (target && [target respondsToSelector:selector]) {
            objc_msgSend(target,selector,status,_audioType,_audioName);
        }
    }

}

- (void)invokeProgressToObserver:(NSTimeInterval)progress {
    for (id target in _observers) {
        SEL selector = @selector(tztAudioPlayerProgressDidUpdated:audioType:audioName:);
        if (target && [target respondsToSelector:selector]) {
            objc_msgSend(target,selector,progress,_audioType,_audioName);
        }
    }
}

- (void)tztAudioPlayerDidGetDuraion:(NSTimeInterval)duration audioType:(tztAudioType)type audioName:(NSString *)name {
    //防止外部勿调用 导致crash
}
- (void)tztAudioPlayerStatusDidChanged:(tztAudioPlayerStatus)status audioType:(tztAudioType)type audioName:(NSString *)name {
    //防止外部勿调用 导致crash
}
- (void)tztAudioPlayerProgressDidUpdated:(NSTimeInterval)progress audioType:(tztAudioType)type audioName:(NSString *)name {
    //防止外部勿调用 导致crash
}
@end


