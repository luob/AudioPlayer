//
//  ViewController.m
//  AudioPlayerDemo
//
//  Created by Lin Zhang on 12-7-5.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "ViewController.h"
#import "AudioCell.h"
#import "tztAudioPlayer.h"

static NSArray *itemArray;

@interface ViewController ()

@end

@implementation ViewController

@synthesize tableView = _tableView;

- (void)dealloc
{
    [super dealloc];
    
    [_tableView release];    
    [_audioPlayer release];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    self.title = @"音乐列表";
    
    itemArray = [[NSArray arrayWithObjects:
                  [NSDictionary dictionaryWithObjectsAndKeys:@"七牛 MP4", @"song", @"五月天", @"artise", @"http://yanlj.qiniudn.com/111.mp3", @"url",@"1",@"type", nil],
                  [NSDictionary dictionaryWithObjectsAndKeys:@"重定向 Mp3", @"song", @"刘德华", @"artise", @"http://sc1.111ttt.com:8282/2015/1/11/18/104182350371.mp3", @"url",@"1",@"type", nil],
                  [NSDictionary dictionaryWithObjectsAndKeys:@"不能播放 Mp3", @"song", @"陈奕迅", @"artise", @"http://58.213.69.37:48080/wiki/aa36842b8241e7488693ee0eddfbd7b4.mp3", @"url",@"1",@"type", nil],
                  [NSDictionary dictionaryWithObjectsAndKeys:@"无AccessRange 能播 Mp3", @"song", @"五月天", @"artise", @"http://d.zhangle.com/pic/cft/interaction/288988b651495e150151617ce5b6025e/288988b651495e150151617ce5b6025e.mp3", @"url",@"1",@"type", nil],
                  [NSDictionary dictionaryWithObjectsAndKeys:@"直播:完全正常能播 Mp3", @"song", @"任贤齐", @"artise", @"http://d.zhangle.com/pic/cft/interaction/288988b7519056900151999e1b0b0059/288988b7519056900151999e1b0b0059.mp3", @"url",@"2",@"type",@"0",@"seek", nil],
                    [NSDictionary dictionaryWithObjectsAndKeys:@"直播:seek5s conveted Mp3", @"song", @"任贤齐", @"artise", @"http://yanlj.qiniudn.com/converted.mp3", @"url",@"2",@"type",@"5",@"seek", nil],
                    [NSDictionary dictionaryWithObjectsAndKeys:@"直播:seek10s converted2 Mp3", @"song", @"任贤齐", @"artise", @"http://yanlj.qiniudn.com/converted2.mp3", @"url",@"2",@"type",@"10",@"seek", nil],
                 nil] retain];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    self.tableView = nil;
}

- (void)playAudio:(AudioButton *)button
{    
    NSInteger index = button.tag;
    NSDictionary *item = [itemArray objectAtIndex:index];
    
    _audioPlayer = [tztAudioPlayer sharedInstance];
    
    if (![[_audioPlayer getAudioName] isEqualToString:[item objectForKey:@"url"]]) {
        [_audioPlayer prepareToPlayWithUrl:[item objectForKey:@"url"] audioType:[[item objectForKey:@"type"] intValue] audioName:[item objectForKey:@"url"]];
        if ([[item objectForKey:@"type"] intValue] == tztAudioTypeLive) {
            [_audioPlayer playWithStartTime:[[item objectForKey:@"seek"] doubleValue]];
        } else {
            [_audioPlayer play];
        }
        [_audioPlayer addObserver:self];

    } else {
        if (_audioPlayer.status == tztAudioPlayerStatusPlaying) {
            [_audioPlayer pause];
            [_audioPlayer removeObserver:self];
        } else {
            if ([[item objectForKey:@"type"] intValue] == tztAudioTypeLive) {
                [_audioPlayer playWithStartTime:[[item objectForKey:@"seek"] doubleValue]];
            } else {
                [_audioPlayer play];
            }
            [_audioPlayer addObserver:self];
        }
    }
}

- (void)sliderValueChanged:(UISlider *)slider {
}

- (void)sliderTouchUpInsider:(UISlider *)slider {
    NSTimeInterval time = slider.value * _audioPlayer.duration;
    [_audioPlayer seekToTime:time completion:^(BOOL success) {
        if (success) {
            NSLog(@"seek success");
        } else {
            NSLog(@"seek failure");
        }
    }];
}

- (void)tztAudioPlayerDidGetDuraion:(NSTimeInterval)duration audioType:(tztAudioType)type audioName:(NSString *)name {
    NSLog(@"duration:%lf",duration);
}

- (void)tztAudioPlayerStatusDidChanged:(tztAudioPlayerStatus)status audioType:(tztAudioType)type audioName:(NSString *)name {
    NSLog(@"tztAudioPlayerStatus:%d -type:%d -name:%@",status,type,name);
    if (status == tztAudioPlayerStatusFinished) {
        [_audioPlayer resetPlayer];
    }
}

- (void)tztAudioPlayerProgressDidUpdated:(NSTimeInterval)progress audioType:(tztAudioType)type audioName:(NSString *)name {
    NSLog(@"Progress:%lf -type:%d -name:%@",progress,type,name);
    NSLog(@"status---:%d",_audioPlayer.status);
    NSLog(@"duration---:%lf",_audioPlayer.duration);
    int hour = progress/3600;
    int minute = (progress - hour * 3600)/60;
    int second = progress - hour * 3600 - minute * 60;
    NSLog(@"%d:%d:%d",hour,minute,second);
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

#pragma mark
#pragma mark - UITableView Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [itemArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70.f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"AudioCell";
    
    AudioCell *cell = (AudioCell *)[self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:self options:nil];
        cell = (AudioCell *)[nibArray objectAtIndex:0];
        [cell configurePlayerButton];
    }
    
    // Configure the cell..
    NSDictionary *item = [itemArray objectAtIndex:indexPath.row];
    
    cell.titleLabel.text = [item objectForKey:@"song"];
    cell.artistLabel.text = [item objectForKey:@"artise"];
    cell.audioButton.tag = indexPath.row;
    [cell.audioButton addTarget:self action:@selector(playAudio:) forControlEvents:UIControlEventTouchUpInside];    
    
    cell.slider.tag = indexPath.row;
    [cell.slider addTarget:self action:@selector(sliderValueChanged:) forControlEvents:UIControlEventValueChanged];
    [cell.slider addTarget:self action:@selector(sliderTouchUpInsider:) forControlEvents:UIControlEventTouchUpInside];

    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
//    [_audioPlayer stop];
//    [_audioPlayer seekToTime:15 completion:^(BOOL success) {
//        if (success) {
//            NSLog(@"seek success");
//        } else {
//            NSLog(@"seek failure");
//        }
//    }];
//    NSLog(@"seek ....");
}

@end
