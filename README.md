### AudioPlayer

An online iOS audio player based on AVAudioStreamer with a custom UI.

#### How to use
1. link CFNetwork.framework, AudioToolbox.framework, QuartCore.framework, CoreMedia, CoreMedia.framework
2. import AudioPlayer floder
3. create an instance of AudioPlayer with AudioButton and url, like below:
 
```  
- (void)playAudio:(AudioButton *)button
{    
    NSInteger index = button.tag;
    NSDictionary *item = [itemArray objectAtIndex:index];
    
    _audioPlayer = [tztAudioPlayer sharedInstance];
    
    if (![[_audioPlayer getAudioName] isEqualToString:[item objectForKey:@"url"]]) {
        [_audioPlayer prepareToPlayWithUrl:[item objectForKey:@"url"] audioType:[[item objectForKey:@"type"] intValue] audioName:[item objectForKey:@"url"]];
        if ([[item objectForKey:@"type"] intValue] == tztAudioTypeLive) {
            [_audioPlayer playWithStartTime:[[item objectForKey:@"seek"] doubleValue]];
        } else {
            [_audioPlayer play];
        }
        [_audioPlayer addObserver:self];

    } else {
        if (_audioPlayer.status == tztAudioPlayerStatusPlaying) {
            [_audioPlayer pause];
            [_audioPlayer removeObserver:self];
        } else {
            if ([[item objectForKey:@"type"] intValue] == tztAudioTypeLive) {
                [_audioPlayer playWithStartTime:[[item objectForKey:@"seek"] doubleValue]];
            } else {
                [_audioPlayer play];
            }
            [_audioPlayer addObserver:self];
        }
    }
}

```

![screenshots](https://github.com/marshluca/AudioPlayer/raw/master/AudioPlayer/images/screenshots.png)
